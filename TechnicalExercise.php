<?php

//namespace BSG;

/**
 * ServicesContainer interface
 *
 * @package default
 * @author Paul McIntosh
 */
interface ServicesContainer {

	public function addService($key,Services $item);
	
	public function getService($key);
	
	public function setRow($arr);


}

/**
 * Service class implements ServicesContainer
 *
 * @package default
 */
class Service implements ServicesContainer {
	
	public $items;
	
	public $output;
	
	public function __construct($value=''){
				
		$this->setCalendar();
		
	}
	
	public function addService($key = 'foo', Services $item) {

		$this -> items[$key][] = $item;

	}

	public function getService($key = 'foo') {

		return $this -> items[$key];

	}
	
	public function setCalendar(){
								
		for ($i=1; $i < 13; $i++) {
			
			$csv = new CsvService();
						
			$csv->setLastDayOfTheMonth(date('Y'),$i,1);
			
			$csv->day = $csv->getDayFromDate($csv->last_day_of_the_month);
			
			$csv->addItem($csv->last_day_of_the_month);
			
			$csv->setPeriod(date('Y'),$i);
			
			$csv->getBonusDate(date('Y'),$i);
			
			$csv->setBonusDate(date('Y'),$i);
			
			$this->addService('months',$csv);
			
		}
		
	}
	
	public function setRow($arr){
			
		$it = new ArrayIterator($arr);
		
		$it = new CachingIterator($it);
		
		foreach ($it as $el) {
			
			$str = '';
			
			if($it->hasNext()){
				
				$this->output .= $it->current() . ",";				
				
			} else {
				
				$this->output .= $it->current() . ";";
			}
						
		}
		
	}

}

/**
 * Services Interface
 *
 * @package default
 */
interface Services {

	public function addItem($item);
	

}

/**
 * Serve abstract class implements Services
 *
 * @package default
 */
abstract class Serve implements Services {

	protected $service;
	
	public $item = array();

	public function addItem($item) {

		$this->item[] = array('period',$item,'bonus');
		
	}
	


}



/**
 * AddService class
 *
 * @package default
 */
class AddService extends Serve {

}

/**
 * CsvService class
 *
 * @package default
 */
class CsvService extends Serve {
	
	public $csv = array(
		'period: m/y',
		'basic payment: Y-m-d',
		'bonus payment: Y-m-d'
	);
	
	public $month;
	
	public $day;
		
	public $last_day_of_the_month;
	
	public $bonus_day;

	public function setLastDayOfTheMonth($y=2020,$m=1,$d=1){
		
		$date = "$y-$m-$d";
		
		$this->last_day_of_the_month = date("Y-m-t",strtotime($date));
		
	}
	
	public function getDayFromDate($date){
		
		$timestamp = strtotime($date);
		
		return date('D',$timestamp);
	}
	
	public function setPeriod($y,$m){
		
		$this->item[0][0] = $y . "/" . $m;
		
	}
	
	public function getBonusDate($y,$m){
		
		$date = $y . "-" . $m . "-10";
		
		$this->bonus_day = $this->getDayFromDate($date);
				
	}
	
	public function setBonusDate($y,$m){
		
		$date = $y . "-" . $m . "-10";
		
		if($this->bonus_day == 'Sat' || $this->bonus_day == 'Sun'){
			
		$date = $y . "-" . $m . "-12";
						
		}
		
		$this->item[0][2] = $date;
				
	}

}



/**
 * TechnicalExercise class
 *
 * @package default
 */
class TechnicalExercise extends Service{

	public $policy;
	
	public $str;

	public function add($policy=false){
				
		$AddService = new AddService();
		
		$this->getMonths();
		
		//TODO: Remove if in console application
		echo $this->output;
				
	}
	
	public function getMonths(){
		
		$months = $this->getService('months');
		
		foreach ($months as $month) {
			
			$this->setRow($month->item[0]);
			
		}
		
	}



}
?>